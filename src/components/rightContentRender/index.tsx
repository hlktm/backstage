import styles from './index.less';
import Right from '../right';
import Breadcrumb from '../breadcrumb';

export default function IndexPage() {
  return (
    <div className={styles.rightContent}>
      <Right />
      <Breadcrumb />
    </div>
  );
}
