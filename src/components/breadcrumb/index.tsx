import React from 'react';
import { Breadcrumb } from 'antd';

const App: React.FC = (data) => (
  <Breadcrumb>
    <Breadcrumb.Item>
      <a href="/">工作台</a>
    </Breadcrumb.Item>
    {data ? (
      <Breadcrumb.Item>
        <a href="">Application Center</a>
      </Breadcrumb.Item>
    ) : (
      ''
    )}
  </Breadcrumb>
);

export default App;
