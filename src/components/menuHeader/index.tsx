import styles from './index.less';
import { Menu, Dropdown, Space, Button } from 'antd';
import { DownOutlined, SmileOutlined } from '@ant-design/icons';

const menu = (
  <Menu
    items={[
      {
        key: '1',
        label: <a>新建文章-协同编辑器</a>,
      },
      {
        key: '2',
        label: <a> 新建文章</a>,
      },
      {
        key: '3',
        label: <a> 新建页面</a>,
      },
    ]}
  />
);

export default function IndexPage() {
  return (
    <div className={styles.menuHeader}>
      <div className={styles.menuImg}>
        <img
          src="https://wipi.oss-cn-shanghai.aliyuncs.com/2021-02-20/wipi-favicon.png"
          alt=""
        />
        <h2>后台管理</h2>
      </div>
      <div className={styles.menuBtn}>
        <Dropdown overlay={menu} arrow={{ pointAtCenter: true }}>
          <Space>
            <Button type="primary" block>
              + 新建
            </Button>
            <DownOutlined />
          </Space>
        </Dropdown>
      </div>
    </div>
  );
}
