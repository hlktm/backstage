import React from 'react';
import { RequestConfig } from 'umi';

import {
  BasicLayoutProps,
  Settings as LayoutSettings,
} from '@ant-design/pro-layout';
import Footer from './components/footer';
import MenuHeader from './components/menuHeader';
import RightContentRender from './components/rightContentRender';

export const layout = () => {
  return {
    headerRender: () => (
      <>
        <RightContentRender />
      </>
    ),
    footerRender: () => <Footer />,
    menuHeaderRender: () => <MenuHeader />,
    onPageChange: () => {},
  };
};

export const request: RequestConfig = {
  timeout: 1000,
  errorConfig: {},
  middlewares: [],
  requestInterceptors: [],
  responseInterceptors: [],
};
