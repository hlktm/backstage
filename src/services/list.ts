import { request } from 'umi';

interface Params {
  i18n: string;
  systemUrl: string;
  adminSystemUrl: string;
  systemTitle: string;
  systemLogo: string;
  systemFavicon: string;
  systemFooterInfo: string;
  seoKeyword: string;
  seoDesc: string;
  baiduAnalyticsId: string;
  googleAnalyticsId: null;
}

export const list = () => {
  request('/api/setting/get', {
    method: 'get',
    skipErrorHandler: true,
  });
};
