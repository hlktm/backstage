// config/routes.ts

export default [
  {
    exact: true,
    path: '/',
    name: '工作台',
    component: '@/pages/index',
    icon: 'dashboard',
  },
  {
    path: '/article',
    name: '文章管理',
    icon: 'form',
    routes: [
      {
        exact: true,
        path: '/article/allarticles',
        name: '所有文章',
        component: '@/pages/article/allarticles',
        icon: 'edit',
      },
      {
        exact: true,
        path: '/article/sortclassify',
        name: '分类管理',
        component: '@/pages/article/sortclassify',
        icon: 'copy',
      },
      {
        exact: true,
        path: '/article/allarticle',
        name: '标签管理',
        component: '@/pages/article/labels',
        icon: 'tag',
      },
    ],
  },
  {
    exact: true,
    path: '/pages',
    name: '页面管理',
    component: '@/pages/pages',
    icon: 'snippets',
  },
  {
    exact: true,
    path: '/knowledge',
    name: '知识小册',
    component: '@/pages/knowledge',
    icon: 'book',
  },
  {
    exact: true,
    path: '/posters',
    name: '海报管理',
    component: '@/pages/posters',
    icon: 'star',
  },
  {
    exact: true,
    path: '/comments',
    name: '评论管理',
    component: '@/pages/comments',
    icon: 'message',
  },
  {
    exact: true,
    path: '/email',
    name: '邮件管理',
    component: '@/pages/email',
    icon: 'mail',
  },
  {
    exact: true,
    path: '/files',
    name: '文件管理',
    component: '@/pages/files',
    icon: 'folderOpen',
  },
  {
    exact: true,
    path: '/search',
    name: '搜索记录',
    component: '@/pages/search',
    icon: 'search',
  },
  {
    exact: true,
    path: '/access',
    name: '访问统计',
    component: '@/pages/access',
    icon: 'project',
  },
  {
    exact: true,
    path: '/user',
    name: '用户管理',
    component: '@/pages/user',
    icon: 'user',
  },
  {
    exact: true,
    path: '/system',
    name: '系统设置',
    component: '@/pages/system',
    icon: 'setting',
  },
  {
    exact: true,
    path: '/personal',
    name: '个人中心',
    component: '@/pages/personal',
    // 隐藏自己和子菜单
    hideInMenu: true,
  },

  {
    exact: true,
    path: '/login',
    component: '@/pages/login',
    name: '登录',
    // 新页面打开
    target: '_blank',
    // 不展示顶栏
    headerRender: false,
    // 不展示页脚
    footerRender: false,
    // 不展示菜单
    menuRender: false,
    // 不展示菜单顶栏
    menuHeaderRender: false,
    // 权限配置，需要与 plugin-access 插件配合使用
    access: 'canRead',
    // 隐藏子菜单
    hideChildrenInMenu: true,
    // 隐藏自己和子菜单
    hideInMenu: true,
    // 在面包屑中隐藏
    hideInBreadcrumb: true,
    // 子项往上提，仍旧展示,
    flatMenu: true,
  },
];
